<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');
Route::post('/enviar_dados_trabalhe_conosco', 'HomeController@store_trabalhe_conosco');
Route::post('/enviar_dados_contato', 'HomeController@contact_store');


Route::middleware(['auth'])->group( function(){
    
    Route::prefix('dashboard')->group( function() {
        
        Route::get('/', 'Dashboard\dashboardController@index')->name('dashboard');
        Route::get('/logout', 'Dashboard\dashboardController@logout');

        
        Route::prefix('editar_site')->group( function() {
            
            Route::get('/', 'Dashboard\EditarSiteController@index')->name('editar_site_index');

            Route::get('/editar_vitrine/{id}', 'Dashboard\EditarSiteController@edit_vitrine');
            Route::post('/save_vitrine', 'Dashboard\EditarSiteController@save_vitrine');
            Route::delete('/remover_slide/{id}', 'Dashboard\EditarSiteController@destroy_slide');
            Route::put('/atualizar_slide/{id}', 'Dashboard\EditarSiteController@update_slide');

            Route::get('/editar_portfolio/{id}', 'Dashboard\EditarSiteController@edit_portfolio');

            Route::prefix('editar_clientes')->group( function(){

                Route::get('/', 'Dashboard\EditarSiteController@index_cliente');
                Route::get('/cliente_tipo/{id}', 'Dashboard\EditarSiteController@edit_cliente');
                Route::post('/save_client', 'Dashboard\EditarSiteController@store_cliente');
                Route::put('/atualizar_cliente/{id}', 'Dashboard\EditarSiteController@update_client');
                Route::delete('/remover_cliente/{id}', 'Dashboard\EditarSiteController@destroy_client');

            });

            Route::get('/editar_contatos/{id}', 'Dashboard\EditarSiteController@edit_contato');

        });

        Route::prefix('perfil')->group( function(){

            Route::get('/', 'Dashboard\PerfilController@index');
            Route::get('/listar_perfis', 'Dashboard\PerfilController@list');
            Route::get('/cadastrar_perfil', 'Dashboard\PerfilController@create');

        });

        Route::prefix('trabalhe_conosco')->group( function(){

            Route::get('/', 'Dashboard\TrabalheConoscoController@index');
            Route::get('/listagem', 'Dashboard\TrabalheConoscoController@list');

        });

        Route::prefix('contato_recebido')->group( function(){

            Route::get('/', 'Dashboard\ContatoRecebidoController@index');
            Route::get('/listar_contatos', 'Dashboard\ContatoRecebidoController@contact_list');

        });

    });

});


Route::get('/registrar', 'Auth\RegisterController@create');
Route::post('/registrar_dados', 'Auth\RegisterController@store');
Route::get('/autenticar', 'Auth\LoginController@create');
Route::post('/autenticar_dados', 'Auth\LoginController@check');