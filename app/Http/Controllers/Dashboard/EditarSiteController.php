<?php

namespace App\Http\Controllers\Dashboard;

use App\Model\Cliente;
use App\Model\Vitrine;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class EditarSiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('dashboard.editar_site.index');
    }

    public function index_cliente(){
        return view('dashboard.editar_site.index_cliente');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function save_vitrine(Request $request){

        if($request->hasFile('file')){

            $file_name = 'slyde_' . str_random(15) . '.jpg';
            
            Storage::disk('public')->putFileAs('/', $request->file, $file_name);

            Vitrine::create([
                'file' => 'images/' . $file_name,
                'link' => $request->link,
                'title' => $request->title
            ]);

            return redirect()->action('Dashboard\EditarSiteController@edit_vitrine', '1');

        } else {
            return "Você carregou nenhum arquivo";            
        }

    }

    public function store_cliente(Request $request){

        if($request->hasFile('file')){

            if ($request->type == '1') {
                $type = 'business';
            } else if ($request->type == '2') {
                $type = 'integradores';
            } else if ($request->type == '3') {
                $type = 'fabricantes';
            } else if ($request->type == '4') {
                $type = 'outros';
            } 

            $file_name = $type . '/' . $type . '-' . ($request->size + 1) . '.' . $request->file->getClientOriginalExtension();
            
            Storage::disk('public')->putFileAs('/' . $type, $request->file, $file_name);

            Cliente::create([
                'type' => $type,
                'path_file' => 'images/' . $file_name
            ]);

            return redirect()->action('Dashboard\EditarSiteController@edit_cliente', $request->type);

        } else {
            return "Você carregou nenhum arquivo";            
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        //return view('dashboard.editar_site.edit_vitrine');
    }

    public function edit_vitrine($id){

        $vitrines = Vitrine::all();

        return view('dashboard.editar_site.edit_vitrine', compact('vitrines'));
    }

    public function edit_portfolio($id){
        return view('dashboard.editar_site.edit_portfolio');
    }

    public function edit_cliente($id){

        if ($id == 1) {
            $clientes = Cliente::where('type','business')->get();
        } else if ($id == 2) {
            $clientes = Cliente::where('type','integradores')->get();
        } else if ($id == 3) {
            $clientes = Cliente::where('type','fabricantes')->get();
        } else if ($id == 4) {
            $clientes = Cliente::where('type','outros')->get();
        }

        return view('dashboard.editar_site.edit_cliente', compact('clientes','id'));
    }

    public function edit_contato($id){
        return view('dashboard.editar_site.edit_contato');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        //
    }

    public function update_slide($id){
        return $id;
    }

    public function update_cliente($id){

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        //
    }

    public function destroy_slide($id){
        
        Vitrine::destroy($id);

        return redirect()->action('Dashboard\EditarSiteController@edit_vitrine', '1');
    }

    public function destroy_client($id){

        $cliente = Cliente::find($id);

        Cliente::destroy($id);

        if ($cliente->type == 'business') {
            $tipo = 1;
        } else if ($cliente->type == 'integradores') {
            $tipo = 2;
        } else if ($cliente->type == 'fabricantes') {
            $tipo = 3;
        } else if ($cliente->type == 'outros') {
            $tipo = 4;
        }

        return redirect()->action('Dashboard\EditarSiteController@edit_cliente', $tipo);
    }

}
