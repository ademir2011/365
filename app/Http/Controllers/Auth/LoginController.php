<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Model\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    protected function create(){
        return view('account.login');
    }

    function data_user($request){
        return array(
            'email' => $request->email,
            'password' => $request->password
        );
    }

    protected function check(Request $request){
    
        $user = User::where('email', $request->email)->first();
        
        if(empty($user)){

            return "não existe";

        } else {

            if ( password_verify($request->password, $user->password ) ) {

                if(Auth::attempt( $this->data_user($request) )){

                    return redirect()->route('dashboard');

                } else {
                    return "não foi possível salvar sua conta";
                }

            } else {

                return "Senha inválida";
    
            }

        }

    }

}
