<?php

namespace App\Http\Controllers;

use App\Model\Cliente;
use App\Model\Contato;
use App\Model\Interessado;
use App\Model\Vitrine;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $vitrines = Vitrine::all();

        $clientes_business = Cliente::where('type','business')->get();
        $clientes_integradores = Cliente::where('type','integradores')->get();
        $clientes_fabricantes = Cliente::where('type','fabricantes')->get();
        $clientes_outros = Cliente::where('type','outros')->get();

        return view('index', compact('vitrines','clientes_business','clientes_integradores','clientes_fabricantes','clientes_outros'));
    }

    public function store_trabalhe_conosco(Request $request){

        Interessado::create($request->all());

        return redirect()->action('HomeController@index');

    }

    public function contact_store(Request $request){

        Contato::create($request->all());

        return redirect()->action('HomeController@index');
    }

}
