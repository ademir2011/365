<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Interessado extends Model
{
    protected $fillable = [
        'name', 'date', 'email', 'phone', 'area',
    ];
}
