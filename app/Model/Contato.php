<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $fillable = [
        'tema', 'name', 'message', 'email', 'phone',
    ];
}
