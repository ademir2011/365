<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vitrine extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file', 'link', 'title',
    ];
}
