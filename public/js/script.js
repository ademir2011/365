(function ($, window) {
    $('#owl-solucao').owlCarousel({
        loop: false,
        center: false,
        autoplay: false,
        margin: 0,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    $('#owl-slyde').owlCarousel({
        loop: true,
        center: false,
        autoplay: true,
        margin: 30,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    $('.owl-carousel').owlCarousel({
        loop: true,
        center: false,
        autoplay: true,
        margin: 30,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });

    $( "#btn-avancar").click(function() {
        $( "#solucao-item-1").fadeOut( 1, function() {});
        $( "#solucao-item-2").fadeIn( 1000, function () {});
    });
    $("#btn-voltar").click(function () {
        $("#solucao-item-2").fadeOut(1, function () { });
        $("#solucao-item-1").fadeIn(1000, function () { });
    });

    $("#solu-1").click(function () {
        $("#fig-1").toggle();
        $("#solu-1").toggle();
    });
    $("#fig-1").click(function () {
        $("#fig-1").toggle();
        $("#solu-1").toggle();
    });
    $("#solu-2").click(function () {
        $("#fig-2").toggle();
        $("#solu-2").toggle();
    });
    $("#fig-2").click(function () {
        $("#fig-2").toggle();
        $("#solu-2").toggle();
    });
    $("#solu-3").click(function () {
        $("#fig-3").toggle();
        $("#solu-3").toggle();
    });
    $("#fig-3").click(function () {
        $("#fig-3").toggle();
        $("#solu-3").toggle();
    });
    $("#solu-4").click(function () {
        $("#fig-4").toggle();
        $("#solu-4").toggle();
    });
    $("#fig-4").click(function () {
        $("#fig-4").toggle();
        $("#solu-4").toggle();
    });
    $("#solu-5").click(function () {
        $("#fig-5").toggle();
        $("#solu-5").toggle();
    });
    $("#fig-5").click(function () {
        $("#fig-5").toggle();
        $("#solu-5").toggle();
    });
    $("#solu-6").click(function () {
        $("#fig-6").toggle();
        $("#solu-6").toggle();
    });
    $("#fig-6").click(function () {
        $("#fig-6").toggle();
        $("#solu-6").toggle();
    });

    $( "#btn-avancar-1" ).click(function() {
        $( "#item-1" ).fadeOut( 1, function() {});
        $( "#item-2" ).fadeIn( 1000, function() {});
        $( "#item-3" ).fadeOut( 1, function() {});
        $( "#item-4" ).fadeOut( 1, function() {});
    });
    $( "#btn-avancar-2" ).click(function() {
        $( "#item-1" ).fadeOut( 1, function() {});
        $( "#item-2" ).fadeOut( 1, function() {});
        $( "#item-3" ).fadeIn( 1000, function() {});
        $( "#item-4" ).fadeOut( 1, function() {});
    });
    $( "#btn-avancar-3" ).click(function() {
        $( "#item-1" ).fadeOut( 1, function() {});
        $( "#item-2" ).fadeOut( 1, function() {});
        $( "#item-3" ).fadeOut( 1, function() {});
        $( "#item-4" ).fadeIn( 1000, function() {});
    });
    $( "#btn-voltar-4" ).click(function() {
        $( "#item-1" ).fadeOut( 1, function() {});
        $( "#item-2" ).fadeOut( 1, function() {});
        $( "#item-3" ).fadeIn( 1000, function() {});
        $( "#item-4" ).fadeOut( 1, function() {});
    });
    $( "#btn-voltar-3" ).click(function() {
        $( "#item-1" ).fadeOut( 1, function() {});
        $( "#item-2" ).fadeIn( 1000, function() {});
        $( "#item-3" ).fadeOut( 1, function() {});
        $( "#item-4" ).fadeOut( 1, function() {});
    });
    $( "#btn-voltar-2" ).click(function() {
        $( "#item-1" ).fadeIn( 1000, function() {});
        $( "#item-2" ).fadeOut( 1, function() {});
        $( "#item-3" ).fadeOut( 1, function() {});
        $( "#item-4" ).fadeOut( 1, function() {});
    });

    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });
    });
})(jQuery, window);