-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: project365
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'business','/images/business/business-1.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(2,'business','/images/business/business-2.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(3,'business','/images/business/business-3.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(4,'business','/images/business/business-4.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(5,'business','/images/business/business-5.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(6,'business','/images/business/business-6.jpeg','2018-05-03 08:58:56','2018-05-03 08:58:56'),(7,'integradores','/images/integradores/integradores-1.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(8,'integradores','/images/integradores/integradores-2.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(9,'integradores','/images/integradores/integradores-3.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(10,'integradores','/images/integradores/integradores-4.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(11,'integradores','/images/integradores/integradores-5.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(12,'integradores','/images/integradores/integradores-6.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(13,'fabricantes','/images/fabricantes/fabricantes-1.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(14,'fabricantes','/images/fabricantes/fabricantes-2.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(15,'fabricantes','/images/fabricantes/fabricantes-3.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(16,'fabricantes','/images/fabricantes/fabricantes-4.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(17,'fabricantes','/images/fabricantes/fabricantes-5.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(18,'fabricantes','/images/fabricantes/fabricantes-6.jpeg','2018-05-03 08:58:56','2018-05-03 08:58:56'),(19,'outros','/images/outros/outros-1.jpeg','2018-05-03 08:58:56','2018-05-03 08:58:56'),(20,'outros','/images/outros/outros-2.jpeg','2018-05-03 08:58:56','2018-05-03 08:58:56'),(21,'outros','/images/outros/outros-3.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(22,'outros','/images/outros/outros-4.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(23,'outros','/images/outros/outros-5.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(24,'outros','/images/outros/outros-6.jpeg','2018-05-03 08:58:56','2018-05-03 08:58:56'),(25,'outros','/images/outros/outros-7.png','2018-05-03 08:58:56','2018-05-03 08:58:56'),(26,'outros','/images/outros/outros-8.png','2018-05-03 08:58:57','2018-05-03 08:58:57'),(27,'outros','/images/outros/outros-9.png','2018-05-03 08:58:57','2018-05-03 08:58:57'),(28,'outros','/images/outros/outros-10.png','2018-05-03 08:58:57','2018-05-03 08:58:57'),(29,'outros','/images/outros/outros-11.png','2018-05-03 08:58:57','2018-05-03 08:58:57');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos`
--

DROP TABLE IF EXISTS `contatos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tema` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos`
--

LOCK TABLES `contatos` WRITE;
/*!40000 ALTER TABLE `contatos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interessados`
--

DROP TABLE IF EXISTS `interessados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interessados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interessados`
--

LOCK TABLES `interessados` WRITE;
/*!40000 ALTER TABLE `interessados` DISABLE KEYS */;
/*!40000 ALTER TABLE `interessados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (55,'2014_10_12_000000_create_users_table',1),(56,'2014_10_12_100000_create_password_resets_table',1),(57,'2018_05_01_193817_create_vitrines_table',1),(58,'2018_05_03_003452_create_interessados_table',1),(59,'2018_05_03_032854_create_contatos_table',1),(60,'2018_05_03_044505_create_clientes_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'teste','teste@teste.com','$2y$10$B0e3KtTL0aliMyVMrj0ayOGAkL5gBbwp8K5bBJ4WYP72mggX0XS16','eGyfG7LbdUWMhLaPrIjMPPFUvDZqReRaFDuQBiZAMI5fc653WP5TjKJn1VLK','2018-05-03 08:58:56','2018-05-03 08:58:56');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vitrines`
--

DROP TABLE IF EXISTS `vitrines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vitrines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vitrines`
--

LOCK TABLES `vitrines` WRITE;
/*!40000 ALTER TABLE `vitrines` DISABLE KEYS */;
INSERT INTO `vitrines` VALUES (1,'images/slyde_xgdhsz4Qc07jytx.jpg','link teste','título teste','2018-05-03 09:18:43','2018-05-03 09:18:43'),(3,'images/slyde_xYnI6AYh5KPWu6f.jpg','link teste','título teste','2018-05-03 09:18:53','2018-05-03 09:18:53'),(4,'images/slyde_huSb0mCvjsinTzR.jpg','link teste','título teste','2018-05-03 09:19:22','2018-05-03 09:19:22'),(5,'images/slyde_Y5TKQ4eswGthksi.jpg','link teste','título teste','2018-05-03 09:19:26','2018-05-03 09:19:26');
/*!40000 ALTER TABLE `vitrines` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-03  3:22:35
