<?php

use Illuminate\Database\Seeder;
use App\Model\Vitrine;

class VitrinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vitrine::create([
            'file' => 'images/slyde.jpg',
            'link' => 'www.google.com.br',
            'title' => 'imagem1'
        ]);
    }
}
