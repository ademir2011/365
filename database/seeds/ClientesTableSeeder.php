<?php

use Illuminate\Database\Seeder;
use App\Model\Cliente;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 5; $i++){
            Cliente::create([
                'type' => 'business',
                'path_file' => '/images/business/business-' . $i . '.png'
            ]);
        }

        Cliente::create([
            'type' => 'business',
            'path_file' => '/images/business/business-' . 6 . '.jpeg'
        ]);

        for($i = 1; $i <= 6; $i++){
            Cliente::create([
                'type' => 'integradores',
                'path_file' => '/images/integradores/integradores-' . $i . '.png'
            ]);
        }

        for($i = 1; $i <= 5; $i++){
            Cliente::create([
                'type' => 'fabricantes',
                'path_file' => '/images/fabricantes/fabricantes-' . $i . '.png'
            ]);
        }

        Cliente::create([
            'type' => 'fabricantes',
            'path_file' => '/images/fabricantes/fabricantes-' . 6 . '.jpeg'
        ]);

        for($i = 1; $i <= 11; $i++){
            
            if ($i == 1 || $i == 2 || $i == 6) {
                Cliente::create([
                    'type' => 'outros',
                    'path_file' => '/images/outros/outros-' . $i . '.jpeg'
                ]);
            } else {
                Cliente::create([
                    'type' => 'outros',
                    'path_file' => '/images/outros/outros-' . $i . '.png'
                ]);
            }
            
        }
        
    }
}
