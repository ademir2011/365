<?php

use App\Model\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'teste',
            'email' => 'teste@teste.com',
            'password' => bcrypt('teste')
        ]);
    }
}
