@extends('account.template')

@section('content')

    <br>

    <div class="col s12 m6 offset-m3 l4 offset-l4">

        <form method="post" action="{{ url('/autenticar_dados') }}">

            {{ csrf_field() }}

            <div class="row">
                <div class="input-field col s12 m12 l12">
                    <input type="email" id="email" name="email" value="teste@teste.com">
                    <label for="email">E-mail</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12 m12 l12">
                    <input type="password" id="password" name="password" value="teste">
                    <label for="password">Senha</label>
                </div>
            </div>
            
            <div class="row center">
                <button class="btn waves-effect waves-light" type="submit" name="action">Acessar sistema
                    <i class="material-icons right">send</i>
                </button>
            </div>

        </form>

    </div>

@endsection