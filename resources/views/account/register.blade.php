@extends('dashboard.template')

@section('content')

    <br>

    @include('dashboard.perfil.header')

    <div class="col s12 m6 offset-m3 l4 offset-l4">

        <form method="post" action="{{ url('/registrar_dados') }}">

            {{ csrf_field() }}

            <div class="row">
                <div class="input-field col s12 m12 l12">
                    <input type="text" id="nome" name="name">
                    <label for="nome">Nome</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12 m12 l12">
                    <input type="email" id="email" name="email">
                    <label for="email">E-mail</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12 m12 l12">
                    <input type="password" id="password" name="password">
                    <label for="password">Senha</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12 m12 l12">
                    <input type="password" id="confirm_password" name="confirm_password">
                    <label for="confirm_password">Confirmar senha</label>
                </div>
            </div>
            
            <div class="row center">
                <button class="btn waves-effect waves-light" type="submit" name="action">Enviar
                    <i class="material-icons right">send</i>
                </button>
            </div>

        </form>

    </div>

@endsection