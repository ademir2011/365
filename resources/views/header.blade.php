<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>365TI</title>

        <link rel="icon" href="assets/images/favicon.png" type="image/x-icon" />
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">

        <link rel='stylesheet' href="{{ asset('/css/bootstrap.min.css')}}" type='text/css' />
        <link rel='stylesheet' href="{{ asset('/css/hover-min.css')}}" type='text/css' />
        <link rel='stylesheet' href="{{ asset('/css/owl.carousel.min.css')}}" type='text/css' />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <link rel='stylesheet' href="{{ asset('/css/style.css')}}" type='text/css' />
        <link rel='stylesheet' href="{{ asset('/css/imagehover.min.css')}}" type='text/css' />
    </head>
    <body>
        <header>
            <div class="container">
                <div class="row">
                    <div class="topo">
                        <a class="en-us" href="#"> <img src="{{ asset('/images/usa.png')}}" >EN-US / </a>
                        <a class="pt-br" href="#">PT-BR <img src="{{ asset('/images/brasil.png')}}" ></a>
                        <a class="trabalhe-conosco" href="#" data-toggle="modal" data-target="#myModalheader"> Trabalhe conosco</a>
                    </div><!-- topo -->
                </div><!-- row -->
            </div><!-- container -->
            <div class="modal fade" id="myModalheader" tabindex="-1" role="dialog" aria-labelledby="myModalheaderLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalheaderLabel">TRABALHE CONOSCO</h4>
                        </div><!-- modal-header -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form method="post" action="/enviar_dados_trabalhe_conosco" enctype="multipart/form-data">
                                        @csrf
                                        <h6> Encontre seu lugar</h6>
                                        <input type="text" class="input" id="nome" placeholder="Joana" name="name">
                                        <input type="text" class="input " id="date" placeholder="Data de Nascimento" onfocus="(this.type='date')" name="date">
                                        <input type="email" class="input " id="Email" placeholder="E-mail" name="email">
                                        <input type="tel" class="input" id="telefone" placeholder="Telefone" name="phone">
                                        <select id="assunto" class="input" name="area">
                                            <option> Área de interesse</option>
                                            <option id="1">Area-x</option> 
                                            <option id="2">Area-x</option>
                                            <option id="3">Area-x</option>
                                            <option id="4">Area-x</option>                              
                                        </select>
                                        <p>Envie seu Curriculo</p>
                                        <a class="btn btn-default btn-form" href="#" role="button">Enviar Doc.</a>
                                        <span class="nome-arquivo">Nome do Doc.</span>
                                        <button class="btn btn-default btn btn-header " type="submit">ENVIAR</button> 
                                    </form>
                                </div><!-- md-6 -->
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <h6>Vagas Disponíveis</h6>
                                    <ul>
                                        <li>vaga 1 - SP </li>
                                        <li>vaga 2 - SP </li>
                                        <li>vaga 3 - SP </li>
                                        <li>vaga 4 - SP </li>
                                        <li>vaga 5 - SP </li>
                                        <li>vaga 6 - SP </li>
                                        <li>vaga 7 - SP </li>
                                        <li>vaga 8 - SP </li>
                                        <li>vaga 9 - SP </li>
                                       
                                    </ul>   
                                </div><!-- md-6 -->
                            </div><!-- row -->
                        </div><!-- modal-body -->
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal -->
            
            <div class="hidden-xs">
                <nav class="navbar navbar-default menu-topo" id="meuMenu">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#"><img class="logo" src="{{ asset('/images/365ti_logo.png')}}" alt="Logo" /></a>
                        </div><!-- .navbar-header -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="">A EMPRESA</a></li>
                                <li><a href="">SOLUÇÕES</a></li>
                                <li><a href="">CLIENTES</a></li>
                                <li><a href="">PORTFOLIO</a></li>
                                <li><a href="">SUPORTE</a></li>
                                <li><a href="">CONTATO</a></li>
                            </ul><!-- .nav -->
                        </div><!-- .navbar-collapse -->
                    </div><!-- container -->
                </nav><!-- .navbar -->
            </div><!-- hidden-xs -->
            
            <div class="visible-xs">
                <div class="wrapper">
                    <nav id="sidebar">
                        <div id="dismiss">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </div><!-- #dismiss -->
                        <div class="sidebar-header">
                            <h3>MENU</h3>
                        </div><!-- sidebar-header -->
                        <ul class="list-unstyled components">
                            <li><a href="">A EMPRESA</a></li>
                            <li><a href="">SOLUÇÕES</a></li>
                            <li><a href="">CLIENTES</a></li>
                            <li><a href="">PORTFOLIO</a></li>
                            <li><a href="">SUPORTE</a></li>
                            <li><a href="">CONTATO</a></li>
                        </ul>
                    </nav>
                    <div id="content">
                        <nav class="navbar navbar-default">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" id="sidebarCollapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#"><img class="logo" src="{{ asset('/images/365ti_logo.png')}}" alt="Logo" /></a>
                            </div><!-- .navbar-header -->
                        </nav>
                    </div><!-- #content -->
                    <div class="overlay"></div>
                </div><!-- wrapper -->
            </div><!-- visible-xs -->
        </header>