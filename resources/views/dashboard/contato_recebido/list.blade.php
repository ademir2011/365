@extends('dashboard.template')

@section('content')

    <br>

    @include('dashboard.contato_recebido.header')

    <div class="divider"></div>

    <br>

    <div class="row">

        <div class="col s12 m6 offset-m3 l6 offset-l3">

            <table class="responsive-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Data</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Area</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($contatos as $contato)
                    <tr>
                        <td>
                            {{ $contato->id }}
                        </td>
                        <td>
                            {{ $contato->tema }}  
                        </td>
                        <td>
                            {{ $contato->name }}
                        </td>
                        <td>
                            {{ $contato->message }}  
                        </td>
                        <td>
                            {{ $contato->email }} 
                        </td>
                        <td>
                            {{ $contato->phone }} 
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>

@endsection