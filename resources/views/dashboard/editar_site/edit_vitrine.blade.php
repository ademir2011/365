@extends('dashboard.template')

@section('content')

    <br>

    @include('dashboard.editar_site.header')

    <div class="divider"></div>

    <br>

    <div class="row">

        <div class="col s12 m10 offset-m1 l10 offset-l1">

            <div class="row">
                <div class="col s12 m12 l12">
                    <h3>Adicionar nova imagem</h3>
                </div>
            </div>

            <div class="row">

                <form method="post" action="/dashboard/editar_site/save_vitrine" enctype="multipart/form-data">
                    
                    @csrf

                    <div class="col s12 m12 l12">
                        <table class="responsive-table">
                            <thead>
                                <tr>
                                    <th>Imagem</th>
                                    <th>Carregar nova imagem</th>
                                    <th>Título</th>
                                    <th>Link</th>
                                    <th>Salvar</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr id="slide">
                                    <td><img id="update_image" src="" style="width:300px;" class="responsive-img"></td>
                                    <td>
                                        <div class="file-field input-field">
                                            <div class="btn">
                                                <span>File</span>
                                                <input type="file" id="file" name="file">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" value="">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-field">
                                            <input type="text" id="title" name="title" value="título teste">
                                            <label for="title">Título</label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-field">
                                            <input type="text" id="link" name="link" value="link teste">
                                            <label for="link">Link</label>
                                        </div>
                                    </td>
                                    <td>
                                        <button class="btn waves-effect waves-light" type="submit" name="action">Salvar
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </form> 

            </div>

            <div class="row">
                <div class="col s12 m12 l12">
                    <h3>Lista de imagens da vitrine</h3>
                </div>
            </div>

            <div class="row">

                <div class="col s12 m12 l12">
                    <table class="responsive-table">
                        <thead>
                            <tr>
                                <th>Imagem</th>
                                <th>Carregar nova imagem</th>
                                <th>Título</th>
                                <th>Link</th>
                                <th>Salvar</th>
                                <th>Remover</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($vitrines as $vitrine)
                            <tr id="slide">
                                <td><img src="{{ asset($vitrine->file) }}" style="width:300px;" class="responsive-img"></td>
                                <td>
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>File</span>
                                            <input type="file" name="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" value="">
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-field">
                                        <input type="text" id="title" name="title" value="{{ $vitrine->title }}">
                                        <label for="title">Título</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-field">
                                        <input type="text" id="link" name="link" value="{{ $vitrine->link }}">
                                        <label for="link">Link</label>
                                    </div>
                                </td>
                                <form method="post" action="/dashboard/editar_site/atualizar_slide/{{$vitrine->id}}" enctype="multipart/form-data">
                                    @csrf
                                    <input name="_method" type="hidden" value="PUT">
                                    <td>
                                        <button class="btn waves-effect waves-light" type="submit">Atualizar
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </td>
                                </form>
                                <form method="post" action="/dashboard/editar_site/remover_slide/{{$vitrine->id}}" enctype="multipart/form-data">
                                    @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                    <td>
                                        <button class="btn waves-effect waves-light" type="submit">Remover
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </td>
                                </form>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            
        </div>

    </div>

    <script>

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#update_image').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#file").change(function() {
            readURL(this);
        });

    </script>

    

@endsection