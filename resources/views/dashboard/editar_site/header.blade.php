<div class="row center">

    <div class="col s12 m3 l3">
        <a href="{{ url('dashboard/editar_site/editar_vitrine/1') }}" class="waves-effect waves-light btn-large" style="display: block;"><i class="material-icons left">filter</i>Vitrine</a>
    </div>

    <div class="col s12 m3 l3">
        <a href="{{ url('dashboard/editar_site/editar_portfolio/1') }}" class="waves-effect waves-light btn-large" style="display: block;"><i class="material-icons left">library_books</i>Portifólio</a>
    </div>

    <div class="col s12 m3 l3">
        <a href="{{ url('dashboard/editar_site/editar_clientes') }}" class="waves-effect waves-light btn-large" style="display: block;"><i class="material-icons left">domain</i>Clientes</a>
    </div>

    <div class="col s12 m3 l3">
        <a href="{{ url('dashboard/editar_site/editar_contatos/1') }}" class="waves-effect waves-light btn-large" style="display: block;"><i class="material-icons left">local_phone</i>Contato</a>
    </div>

</div>