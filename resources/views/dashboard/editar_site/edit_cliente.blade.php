@extends('dashboard.template')

@section('content')

    <br>

    @include('dashboard.editar_site.header')

    <div class="divider"></div>

    <br>

    @include('dashboard.editar_site.header_2')

    <div class="divider"></div>

    <br>

    <div class="row">
<
        <div class="col s12 m10 offset-m1 l10 offset-l1">

            <div class="row">
                <div class="col s12 m12 l12">
                    <h3>Adicionar novo cliente</h3>
                </div>
            </div>

            <div class="row">

                <form method="post" action="/dashboard/editar_site/editar_clientes/save_client" enctype="multipart/form-data">
                    
                    @csrf

                    <div class="col s12 m12 l12">
                        <table class="responsive-table">
                            <thead>
                                <tr>
                                    <th>Imagem</th>
                                    <th>Carregar nova imagem</th>
                                    <th>Salvar</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr id="slide">
                                    <td><img id="update_image" src="" style="width:300px;" class="responsive-img"></td>
                                    <td>
                                        <div class="file-field input-field">
                                            <div class="btn">
                                                <span>File</span>
                                                <input type="file" id="file" name="file">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" value="">
                                            </div>
                                        </div>
                                    </td>
                                    <input type="hidden" name="type" value="{{$id}}">
                                    <input type="hidden" name="size" value="{{$clientes->count()}}">
                                    <td>
                                        <button class="btn waves-effect waves-light" type="submit" name="action">Salvar
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </form> 

            </div>

            <div class="row">
                <div class="col s12 m12 l12">
                    <h3>Lista de clientes</h3>
                </div>
            </div>

            <div class="row">

                <div class="col s12 m12 l12">
                    <table class="responsive-table">
                        <thead>
                            <tr>
                                <th>Imagem</th>
                                <th>Carregar nova imagem</th>
                                <th>Salvar</th>
                                <th>Remover</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($clientes as $cliente)
                            <tr id="slide">
                                <td><img src="{{ asset($cliente->path_file) }}" style="width:300px;" class="responsive-img"></td>
                                <td>
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>File</span>
                                            <input type="file" name="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" value="">
                                        </div>
                                    </div>
                                </td>
                                <form method="post" action="/dashboard/editar_site/editar_clientes/atualizar_cliente/{{$cliente->id}}" enctype="multipart/form-data">
                                    @csrf
                                    <input name="_method" type="hidden" value="PUT">
                                    <td>
                                        <button class="btn waves-effect waves-light" type="submit">Atualizar
                                        </button>
                                    </td>
                                </form>
                                <form method="post" action="/dashboard/editar_site/editar_clientes/remover_cliente/{{$cliente->id}}" enctype="multipart/form-data">
                                    @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                    <td>
                                        <button class="btn waves-effect waves-light" type="submit">Remover
                                        </button>
                                    </td>
                                </form>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            
        </div>

        </div>

    <script>

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#update_image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#file").change(function() {
        readURL(this);
    });

    </script>

@endsection