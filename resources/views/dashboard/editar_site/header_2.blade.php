<div class="row center">

    <div class="col s12 m3 l3">
        <a href="{{ url('dashboard/editar_site/editar_clientes/cliente_tipo/1') }}" class="waves-effect waves-light btn-large" style="display: block;">Business Partners</a>
    </div>

    <div class="col s12 m3 l3">
        <a href="{{ url('dashboard/editar_site/editar_clientes/cliente_tipo/2') }}" class="waves-effect waves-light btn-large" style="display: block;">Integradores</a>
    </div>

    <div class="col s12 m3 l3">
        <a href="{{ url('dashboard/editar_site/editar_clientes/cliente_tipo/3') }}" class="waves-effect waves-light btn-large" style="display: block;">Fabricantes</a>
    </div>

    <div class="col s12 m3 l3">
        <a href="{{ url('dashboard/editar_site/editar_clientes/cliente_tipo/4') }}" class="waves-effect waves-light btn-large" style="display: block;">Outros</a>
    </div>

</div>