@extends('dashboard.template')

@section('content')

    <br>

    @include('dashboard.trabalhe_conosco.header')

    <div class="divider"></div>

    <br>

    <div class="row">

        <div class="col s12 m6 offset-m3 l6 offset-l3">

            <table class="responsive-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Data</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Area</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($interessados as $interessado)
                    <tr>
                        <td>
                            {{ $interessado->id }}
                        </td>
                        <td>
                            {{ $interessado->name }}  
                        </td>
                        <td>
                            {{ $interessado->date }}
                        </td>
                        <td>
                            {{ $interessado->email }}  
                        </td>
                        <td>
                            {{ $interessado->phone }} 
                        </td>
                        <td>
                            {{ $interessado->area }} 
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>

@endsection