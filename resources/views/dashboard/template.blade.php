<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">

    <!--Import jQuery before materialize.js-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
    
    <style>
      header, main, footer {
        padding-left: 300px;
      }
      @media only screen and (max-width : 992px) {
        header, main, footer {
          padding-left: 0;
        }
      }
    </style>


    <div class="container-fluid">

      <header>
        <nav>
         <div class="nav-wrapper">
           <a href="#!" class="brand-logo">&nbsp&nbsp&nbsp&nbsp365 Dashboard</a>
           <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
           <ul class="right hide-on-med-and-down">
              <li><a href="{{ url('/dashboard/logout') }}">Sair</a></li>
            </ul>
         </div>
        </nav>

        <ul class="sidenav" id="mobile-demo">
            <li><a href="{{ url('/dashboard/editar_site') }}" class="waves-effect"><i class="material-icons">create</i>Editar site</a></li>
            <li><a href="{{ url('/dashboard/contato_recebido') }}" class="waves-effect"><i class="material-icons">call</i>Contatos recebidos</a></li>
            <li><a href="{{ url('/dashboard/trabalhe_conosco') }}" class="waves-effect"><i class="material-icons">business_center</i>Trabalhe conosco</a></li>
            <li><a href="{{ url('/dashboard/perfil') }}" class="waves-effect"><i class="material-icons">account_box</i>Perfil</a></li>
            <li><a href="{{ url('/dashboard/logout') }}" class="waves-effect"><i class="material-icons">close</i>Sair</a></li>
        </ul>
          
      </header>

      <main>

        <div class="row">

            <div class="col s12 m12 l12">

              <ul id="slide-out" class="sidenav sidenav-fixed">
                <li>
                  <div class="user-view">
                    <div class="background">
                      <img src="https://i.imgur.com/AMf9X7E.jpg" class="responsive-img">
                    </div>
                    <a href="#!user"><img class="circle" src="https://cdn3.iconfinder.com/data/icons/users-6/100/654853-user-men-2-512.png"></a>
                    <a href="#!name"><span class="black-text name">{{ @Auth::user()->name }}</span></a>
                    <a href="#!email"><span class="black-text email">{{ @Auth::user()->email }}</span></a>
                  </div>
                </li>
                <li><a href="{{ url('/dashboard/editar_site') }}" class="waves-effect"><i class="material-icons">create</i>Editar site</a></li>
                <li><a href="{{ url('/dashboard/contato_recebido') }}" class="waves-effect"><i class="material-icons">call</i>Contatos recebidos</a></li>
                <li><a href="{{ url('/dashboard/trabalhe_conosco') }}" class="waves-effect"><i class="material-icons">business_center</i>Trabalhe conosco</a></li>
                <li><a href="{{ url('/dashboard/perfil') }}"  class="waves-effect"><i class="material-icons">account_box</i>Perfil</a></li>
                <li><div class="divider"></div></li>
                <li><a href="{{ url('/dashboard/logout') }}"><i class="material-icons">close</i>Sair</a></li>
              </ul>

            </div>

            <div class="col s12 m12 l12">

                @yield('content')

            </div>

        </div>

      </main>

    </div>

    
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>

    <script>
        $(document).ready(function(){
            $('.sidenav').sidenav();
        });
    </script>

</body>
</html>