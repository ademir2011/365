<div class="row center">

    <div class="col s12 m3 offset-m3 l3 offset-l3">
        <a href="{{ url('dashboard/perfil/listar_perfis') }}" class="waves-effect waves-light btn-large" style="display: block;"><i class="material-icons left">filter</i>Perfis cadastrados</a>
    </div>

    <div class="col s12 m3 l3">
        <a href="{{ url('dashboard/perfil/cadastrar_perfil') }}" class="waves-effect waves-light btn-large" style="display: block;"><i class="material-icons left">library_books</i>Cadastrar novo perfil</a>
    </div>

</div>