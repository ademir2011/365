@extends('dashboard.template')

@section('content')

    <br>

    @include('dashboard.perfil.header')

    <div class="divider"></div>

    <br>

    <div class="row">

        <div class="col s12 m6 offset-m3 l6 offset-l3">

            <table class="responsive-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Editar</th>
                        <th>Remover</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>
                            <div class="input-field">
                                <input type="text" id="id" name="id" value="{{ $user->id }}">
                                <label for="id">ID</label>
                            </div>
                        </td>
                        <td>
                            <div class="input-field">
                                <input type="text" id="name" name="name" value="{{ $user->name }}">
                                <label for="name">Nome</label>
                            </div>
                        </td>
                        <td>
                            <div class="input-field">
                                <input type="text" id="email" name="email" value="{{ $user->email }}">
                                <label for="email">E-mail</label>
                            </div>
                        </td>
                        <td>
                            <button class="btn waves-effect waves-light" type="submit">Atualizar
                                <i class="material-icons right">send</i>
                            </button>
                        </td>
                        <td>
                            <button class="btn waves-effect waves-light" type="submit">Remover
                                <i class="material-icons right">send</i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>

@endsection