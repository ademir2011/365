<footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-9 hidden-xs">
                        <nav class="navbar navbar-default menu-footer" id="meuMenu">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div><!-- .navbar-header -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav lista-footer">
                                    <li><a href="">A EMPRESA</a></li>
                                    <li><a href="">SOLUÇÕES</a></li>
                                    <li><a href="">CLIENTES</a></li>
                                    <li><a href="">PORTFOLIO</a></li>
                                    <li><a href="">SUPORTE</a></li>
                                    <li><a href="">CONTATO</a></li>
                                </ul><!-- .nav -->
                            </div><!-- .navbar-collapse -->
                        </nav><!-- .navbar -->
                        <p>Todos os direitos reservados à 365TI - Serviços Especializados em TI</p>
                   </div><!-- col -->
                    <div class="col-xs-4 visible-xs">
                        <ul class="lista-footer">
                            <li><a href="">A EMPRESA</a></li>
                            <li><a href="">SOLUÇÕES</a></li>
                            <li><a href="">CLIENTES</a></li>
                        </ul>
                    </div><!-- xs-4 -->
                    <div class="col-xs-4 visible-xs">
                        <ul class="lista-footer">
                            <li><a href="">PORTFOLIO</a></li>
                            <li><a href="">SUPORTE</a></li>
                            <li><a href="">CONTATO</a></li>
                        </ul>
                    </div><!-- xs-4 -->
                    <div class="col-md-3 col-sm-3 col-xs-4">
                        <div class="redes">
                           <a href="#"><img class="face-footer" src="{{ asset('/images/facebook.png')}}"></a> 
                           <a href="#"><img class="linkedin-footer" src="{{ asset('/images/linkedin.png')}}"></a> 
                        </div>
                    </div><!-- md-3 -->
                </div><!-- row -->
            </div><!-- container -->
        </footer><!-- footer -->

        <script type='text/javascript' src="{{ asset('/js/jquery.js')}}"></script>
        <script type='text/javascript' src="{{ asset('/js/bootstrap.min.js')}}"></script>
        <script src="https://use.fontawesome.com/6963733f46.js"></script>
        <script type='text/javascript' src="{{ asset('/js/owl.carousel.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type='text/javascript' src="{{ asset('/js/script.js')}}"></script>
    </body>
</html>