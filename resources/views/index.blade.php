@include('header')

    <section id="slyde">
        <div class="owl-carousel owl-theme" id="owl-slyde">
            @foreach($vitrines as $vitrine)
                <div class="item">
                    <a href="{{ $vitrine->link }}"><img class="img-responsive" src="{{ asset($vitrine->file) }}" alt="{{ $vitrine->title }}" /></a>
                </div><!-- item -->
            @endforeach
        </div><!-- owl-slyde -->
    </section><!-- #slyde -->

    <section id="empresa">
        <div class="container">
            <h5>A EMPRESA</h5>
            <p>Fundado em 2015 com sede na cidade de São Paulo, nosso nome diz tudo, estamos 365 dias do ano disponíveis para TI tendo como o principal objetivo a oferta especializada de serviços de TI. Com profissionais altamente qualificados e preparados para atender clientes em três diferentes línguas <span>(Português, Inglês e Espanhol).</span></p>
            <p>Tendo como especialidade soluções <span class="avaya">Avaya</span>, desde Implantação, Suporte, Consultoria e Customização. Atendendo desde pequenas soluções até implantações e integrações críticas. Solicite o contato de um de nossos especialistas para ajudar em seu negócio!</p>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h6>Missão</h6>
                    <p>Garantir a excelência na entrega de serviços de TI, maximizando valor para clientes e usuários.</p>
                </div><!-- md-4 -->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="borda">
                        <h6>Valores</h6>
                        <p>Nosso gerenciamento deve ser em equipe, consistente e focado;</p>
                        <p>Nosso relacionamento com clientes e colaboradores deve ser transparente e baseado na responsabilidade e confiança entre as partes.</p>
                    </div><!-- borda -->
                </div><!-- md-4 -->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h6>Visão</h6>
                    <p>Estar entre os principais players do mercado e ser referência de excelência em serviços de TI.</p>
                </div><!-- md-4 -->
            </div> <!-- row -->
        </div><!-- container -->
    </section><!-- #empresa -->

    <div class="hidden-xs">
        <section id="conteudo">
            <div class="container">
                <div class="flex">
                    <div class="centralizar">
                        <img class="img" src="{{ asset('/images/projeto-preto.png')}}">
                        <div class="central">
                            <span> +50</span>
                            <p> PROJETOS <br> ENTREGUES</p>
                        </div><!-- central -->
                    </div><!-- centralizar -->
                    <div class="centralizar">
                        <img class="img" src="{{ asset('/images/suporte-preto.png')}}">
                        <div class="central">
                            <span> +200h</span>
                            <p> SUPORTE <br> MENSAL</p>
                        </div><!-- central -->
                    </div><!-- centralizar -->
                    <div class="centralizar">
                        <img class="img" src="{{ asset('/images/Parceiros.png')}}">
                        <div class="central">
                            <span> +10</span>
                            <p> PARCEIROS<br> AVAYA</p>
                            <p class="servicos"> (UTILIZAM NOSSOS SERVIÇOS)</p>
                        </div><!-- central -->
                    </div><!-- centralizar -->
                    <div class="centralizar">
                        <img class="img" src="{{ asset('/images/Clientes.png')}}">
                        <div class="central">
                            <p> TEMOS<br>CLIENTES<br>EM</p>
                        </div><!-- central -->
                        <img class="paises" src="{{ asset('/images/paises.png')}}">
                    </div><!-- centralizar -->
                    <div class="centralizar">
                        <img class="img" src="{{ asset('/images/Equipe.png')}}">
                        <div class="central">
                            <span> +10</span>
                            <p> ANOS DE<br>EXPERIÊNCIA</p>
                        </div><!-- central -->
                    </div><!-- centralizar -->
                </div><!-- flex -->
            </div><!-- container -->
        </section><!-- #conteudo -->
    </div><!-- hidden-xs -->

    <div class="visible-xs">
        <section id="conteudo-mobile">
            <div class="col-xs-12 item-conteudo">
                <img class="img" src="{{ asset('/images/projeto-preto.png')}}">
                <div class="central">
                    <span> +50</span>
                    <p> PROJETOS <br> ENTREGUES</p>
                </div><!-- central -->
            </div><!-- item-conteudo -->
            <div class="col-xs-12 item-conteudo">
                <img class="img" src="{{ asset('/images/suporte-preto.png')}}">
                <div class="central">
                    <span> +200h</span>
                    <p> SUPORTE <br> MENSAL</p>
                </div><!-- central -->
            </div><!-- item-conteudo -->
            <div class="col-xs-12 item-conteudo">
                <img class="img" src="{{ asset('/images/parceiros.png')}}">
                <div class="central">
                    <span> +10</span>
                    <p> PARCEIROS<br> AVAYA</p>
                    <p class="servicos"> (UTILIZAM NOSSOS SERVIÇOS)</p>
                </div><!-- central -->
            </div><!-- item-conteudo -->
            <div class="centralizar item-conteudo">
                <img class="img" src="{{ asset('/images/clientes.png')}}">
                <div class="central">
                    <p> TEMOS<br>CLIENTES<br>EM</p>
                </div><!-- central -->
                <img class="paises" src="{{ asset('/images/paises.png')}}">
            </div><!-- item-conteudo -->
            <div class="centralizar item-conteudo">
                <img class="img" src="{{ asset('/images/equipe.png')}}">
                <div class="central">
                    <span> +10</span>
                    <p> ANOS DE<br>EXPERIÊNCIA</p>
                </div><!-- central -->
            </div><!-- item-conteudo -->
        </section><!-- #conteudo-mobile -->
    </div><!-- visible-xs -->

    <section id="icones">
        <div class="container">
            <div class="icones pull-right">
                <span> Trabalhamos com:</span>
                <img src="{{ asset('/images/avaya.png')}}">
                <img class="icone-vmware" src="{{ asset('/images/vmware.png')}}">
                <img src="{{ asset('/images/microsoft.png')}}">
            </div><!-- icones -->
        </div><!-- container -->
    </section><!-- #icones -->

    <section id="solucoes">
        <div class="container">
            <div class="hidden-xs">
                <div class="row">
                    <h5>SOLUÇÕES</h5>
                    <div class="col-md-4 col-sm-4">
                        <div class="centralizar">
                            <figure class="imghvr-fade" style="background-color: #2A2A2A; width:100%">
                                <div class="alinhar">
                                    <img src="{{ asset('/images/Consultoria.png')}}">
                                    <p>Consultoria</p>
                                </div><!-- alinhar -->
                                <figcaption style="background-color: #2A2A2A;  padding:0px;">
                                    <div class="topo">
                                        <p class="p-topo">Consultoria</p>
                                        <img class="img-topo" src="{{ asset('/images/Consultoria.png')}}">
                                        <ul>
                                            <li>Desenvolvimento e Análise de RFP;</li>
                                            <li>Identificação de processos e melhorias operacionais;</li>
                                            <li>Automatização de processos e Humanização de URA;</li>
                                            <li>Desenvolvimento de relatórios customizados;</li>
                                        </ul>
                                    </div><!-- topo -->
                                </figcaption>
                            </figure>
                        </div><!-- centralizar -->
                    </div><!-- md-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="centralizar">
                            <figure class="imghvr-fade" style="background-color: #2A2A2A; width:100%">
                                <div class="alinhar">
                                    <img src="{{ asset('/images/prevenda.png')}}">
                                    <p>Pré / Vendas</p>
                                </div><!-- alinhar -->
                                <figcaption style="background-color: #2A2A2A; padding:0px;">
                                    <div class="topo">
                                        <p class="p-topo">Pré-Vendas</p>
                                        <img class="img-topo" src="{{ asset('/images/prevenda.png')}}">
                                        <ul>
                                            <li>Analise de Topologia e Contingência (HA e DR)</li>
                                            <li>Apresentação de Solução Técnica;</li>
                                            <li>Preparação de POC e Try&Buy;</li>
                                        </ul>
                                    </div><!-- topo -->
                                </figcaption>
                            </figure>
                        </div><!-- centralizar -->
                    </div><!-- md-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="centralizar">
                            <figure class="imghvr-fade" style="background-color: #2A2A2A; width:100%">
                                <div class="alinhar">
                                    <img src="{{ asset('/images/venda.png')}}">
                                    <p>Vendas</p>
                                </div><!-- alinhar -->
                                <figcaption style="background-color: #2A2A2A;  padding:0px;">
                                    <div class="topo">
                                        <p class="p-topo">Vendas</p>
                                        <img class="img-topo" src="{{ asset('/images/venda.png')}}">
                                        <ul>
                                            <li>Revisão de Projetos;)</li>
                                            <li>Apoio na escolha de Fornecedor;</li>
                                        </ul>
                                    </div><!-- topo -->
                                </figcaption>
                            </figure>
                        </div><!-- centralizar -->
                    </div><!-- md-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="centralizar">
                            <figure class="imghvr-fade" style="background-color: #2A2A2A; width:100%">
                                <div class="alinhar">
                                    <img src="{{ asset('/images/projeto-vermelho.png')}}">
                                    <p>Implantação</p>
                                </div><!-- alinhar -->
                                <figcaption style="background-color: #2A2A2A;  padding:0px;">
                                    <div class="topo">
                                        <p class="p-topo">Implantação</p>
                                        <img class="img-topo" src="{{ asset('/images/projeto-vermelho.png')}}">
                                        <ul>
                                            <li>Implementação;</li>
                                            <li>Gerenciamento de Projetos;</li>
                                            <li>Revisão de projetos implementados;</li>
                                        </ul>
                                    </div><!-- topo -->
                                </figcaption>
                            </figure>
                        </div><!-- centralizar -->
                    </div><!-- md-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="centralizar">
                            <figure class="imghvr-fade" style="background-color: #2A2A2A; width:100%">
                                <div class="alinhar">
                                    <img src="{{ asset('/images/Suporte.png')}}">
                                    <p>Suporte</p>
                                </div><!-- alinhar -->
                                <figcaption style="background-color: #2A2A2A; padding:0px;">
                                    <div class="topo">
                                        <p class="p-topo">Suporte</p>
                                        <img class="img-topo" src="{{ asset('/images/Suporte.png')}}">
                                        <ul>
                                            <li>Suporte Nível 1, 2 e 3;</li>
                                            <li>Técnico Residente;</li>
                                            <li>Interação com Fabricante em Português, Inglês e Espanhol.</li>
                                            <li>Manutenção Preventiva</li>
                                        </ul>
                                    </div><!-- topo -->
                                </figcaption>
                            </figure>
                        </div><!-- centralizar -->
                    </div><!-- md-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="centralizar">
                            <figure class="imghvr-fade" style="background-color: #2A2A2A; width:100%">
                                <div class="alinhar">
                                    <img src="{{ asset('/images/treinamento.png')}}">
                                    <p>Treinamentos</p>
                                </div><!-- alinhar -->
                                <figcaption style="background-color: #2A2A2A;  padding:0px;">
                                    <div class="topo">
                                        <p class="p-topo">Treinamentos</p>
                                        <img class="img-topo" src="{{ asset('/images/treinamento.png')}}">
                                        <ul>
                                            <li>Treinamento de Utilização e Administração para Usuários, Agentes e Supervisores.</li>
                                        </ul>
                                    </div><!-- topo -->
                                </figcaption>
                            </figure>
                        </div><!-- centralizar -->
                    </div><!-- md-4 -->
                </div><!--row -->
            </div><!-- hidden-xs -->

            <div class="visible-xs">
                <div class="owl-carousel owl-theme" id="owl-solucao">
                    <div class="item">
                        <div class="centralizar">
                            <div class="imghvr-fade" style="background-color: #2A2A2A; width:100%">
                                <div class="alinhar" id="solu-1">
                                    <img  src="{{ asset('/images/consultoria.png')}}">
                                    <p>Consultoria</p>
                                </div><!-- alinhar -->
                                <div id="fig-1" class="fig" style="background-color: #2A2A2A;  padding:0px;">
                                    <div class="topo">
                                        <p class="p-topo">Consultoria</p>
                                        <img class="img-topo" src="{{ asset('/images/consultoria.png')}}">
                                        <ul>
                                            <li>Desenvolvimento e Análise de RFP;</li>
                                            <li>Identificação de processos e melhorias operacionais;</li>
                                            <li>Automatização de processos e Humanização de URA;</li>
                                            <li>Desenvolvimento de relatórios customizados;</li>
                                        </ul>
                                    </div><!-- topo -->
                                </div><!-- fig -->
                            </div><!-- imghvr-fade -->
                        </div><!-- centralizar -->
                        <div class="centralizar">
                            <div class="imghvr-fade" style="background-color: #2A2A2A; width:100%">
                                <div class="alinhar" id="solu-2">
                                    <img src="{{ asset('/images/prevenda.png')}}">
                                    <p>Pré-Vendas</p>
                                </div><!-- alinhar -->
                                <div id="fig-2" class="fig" style="background-color: #2A2A2A;  padding:0px;">
                                    <div class="topo">
                                        <p class="p-topo">Pré-Vendas</p>
                                        <img class="img-topo" src="{{ asset('/images/prevenda.png')}}">
                                        <ul>
                                            <li>Analise de Topologia e Contingência (HA e DR)</li>
                                            <li>Apresentação de Solução Técnica;</li>
                                            <li>Preparação de POC e Try&Buy;</li>
                                        </ul>
                                    </div><!-- topo -->
                                </div><!-- fig -->
                            </div><!-- imghvr-fade -->
                        </div><!-- centralizar -->
                        <div class="centralizar">
                            <div class="imghvr-fade" style="background-color: #2A2A2A; width:100%">
                                <div class="alinhar" id="solu-3">
                                    <img src="{{ asset('/images/venda.png')}}">
                                    <p>Vendas</p>
                                </div><!-- alinhar -->
                                <div id="fig-3" class="fig" style="background-color: #2A2A2A;  padding:0px;">
                                    <div class="topo">
                                        <p class="p-topo">Vendas</p>
                                        <img class="img-topo" src="{{ asset('/images/venda.png')}}">
                                        <ul>
                                            <li>Revisão de Projetos;)</li>
                                            <li>Apoio na escolha de Fornecedor;</li>
                                        </ul>
                                    </div><!-- topo -->
                                </div><!-- fig -->
                            </div><!-- imghvr-fade -->
                        </div><!-- centralizar -->
                        <div class="indicar">
                            <img src="{{ asset('/images/arrastar.png')}}"/>
                        </div><!-- indicar -->
                    </div><!-- item -->
                    <div class="item">
                        <div class="centralizar">
                            <div class="imghvr-fade" style="background-color: #2A2A2A; width:100%">
                                <div class="alinhar" id="solu-4">
                                    <img src="{{ asset('/images/projeto-vermelho.png')}}">
                                    <p>Implantação</p>
                                </div><!-- alinhar -->
                                <div id="fig-4" class="fig" style="background-color: #2A2A2A;  padding:0px;">
                                    <div class="topo">
                                        <p class="p-topo">Implantação</p>
                                        <img class="img-topo" src="{{ asset('/images/projeto-vermelho.png')}}">
                                        <ul>
                                            <li>Implementação;</li>
                                            <li>Gerenciamento de Projetos;</li>
                                            <li>Revisão de projetos implementados;</li>
                                        </ul>
                                    </div><!-- topo -->
                                </div><!-- fig -->
                            </div><!-- imghvr-fade -->
                        </div><!-- centralizar -->
                        <div class="centralizar">
                            <div class="imghvr-fade" style="background-color: #2A2A2A; width:100%">
                                <div class="alinhar" id="solu-5">
                                    <img src="{{ asset('/images/suporte.png')}}">
                                    <p>Suporte</p>
                                </div><!-- alinhar -->
                                <div id="fig-5" class="fig" style="background-color: #2A2A2A; padding:0px;">
                                    <div class="topo">
                                        <p class="p-topo">Suporte</p>
                                        <img class="img-topo" src="{{ asset('/images/suporte.png')}}">
                                        <ul>
                                            <li>Suporte Nível 1, 2 e 3;</li>
                                            <li>Técnico Residente;</li>
                                            <li>Interação com Fabricante em Português, Inglês e Espanhol.</li>
                                            <li>Manutenção Preventiva</li>
                                        </ul>
                                    </div><!-- topo -->
                                </div><!-- fig -->
                            </div><!-- imghvr-fade -->
                        </div><!-- centralizar -->
                        <div class="centralizar">
                            <div class="imghvr-fade" style="background-color: #2A2A2A; width:100%">
                                <div class="alinhar" id="solu-6">
                                    <img src="{{ asset('/images/treinamento.png')}}">
                                    <p> Treinamentos</p>
                                </div><!-- alinhar -->
                                <div id="fig-6" class="fig" style="background-color: #2A2A2A;  padding:0px;">
                                    <div class="topo">
                                        <p class="p-topo">Treinamentos</p>
                                        <img class="img-topo" src="{{ asset('/images/suporte.png')}}">
                                        <ul>
                                            <li>Treinamento de Utilização e Administração para Usuários, Agentes e Supervisores.</li>
                                        </ul>
                                    </div><!-- topo -->
                                </div><!-- fig -->
                            </div><!-- imghvr-fade -->
                        </div><!-- centralizar -->
                        <div class="indicar">
                            <img src="{{ asset('/images/arrastar.png')}}"/>
                        </div><!-- indicar -->
                    </div><!-- item -->
                </div><!-- #owl-solucao -->
            </div><!-- visible-xs -->
        </div><!-- container -->
    </section><!-- #soluçoes -->

    <div class="hidden-xs">
        <section id="clientes">
            <div class="container">
                <div class="row">
                    <h5>CLIENTES</h5>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <ul class="nav nav-tabs" role="tablist">
                                <div class="row">
                                    <li role="presentation" class="col-md-3 col-sm-3 active"><a href="#business" aria-controls="business" role="tab" data-toggle="tab">Business Partners</a></li>
                                    <li role="presentation" class="col-md-3 col-sm-3" ><a href="#integradores" aria-controls="integradores" role="tab" data-toggle="tab">Integradores</a></li>
                                    <li role="presentation" class="col-md-3 col-sm-3"><a href="#fabricantes" aria-controls="fabricantes" role="tab" data-toggle="tab">Fabricantes</a></li>
                                    <li role="presentation" class="col-md-3 col-sm-3" ><a href="#outros" aria-controls="outros" role="tab" data-toggle="tab">Outros</a></li>
                                </div><!-- row -->
                            </ul>
                        </div><!-- md-12 -->
                    </div><!-- row -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="business">
                            <div class="owl-carousel">
                                @foreach($clientes_business as $cliente_business)
                                    <div class="item img"><img class="img-responsive" src="{{ asset($cliente_business->path_file)}}"></div>
                                @endforeach
                            </div><!-- owl-carousel -->
                        </div><!-- tab-pane -->
                        <div role="tabpanel" class="tab-pane fade" id="integradores">
                            <div class="owl-carousel">
                                @foreach($clientes_integradores as $cliente_integradores)
                                    <div class="item img"><img class="img-responsive" src="{{ asset($cliente_integradores->path_file)}}"></div>
                                @endforeach
                            </div><!-- owl-carousel -->
                        </div><!-- tab-pane -->
                        <div role="tabpanel" class="tab-pane fade" id="fabricantes">
                           <div class="owl-carousel">
                                @foreach($clientes_fabricantes as $cliente_fabricantes)
                                    <div class="item img"><img class="img-responsive" src="{{ asset($cliente_fabricantes->path_file)}}"></div>
                                @endforeach
                            </div><!-- owl-carousel -->
                        </div><!-- tab-pane -->
                        <div role="tabpanel" class="tab-pane " id="outros">
                            <div class="owl-carousel">
                                @foreach($clientes_outros as $cliente_outros)
                                    <div class="item img"><img class="img-responsive" src="{{ asset($cliente_outros->path_file)}}"></div>
                                @endforeach
                            </div><!-- owl-carousel -->
                        </div><!-- tab-pane -->
                    </div><!-- tab-content -->
                </div><!-- row -->
            </div><!-- container -->
        </section><!-- #clientes -->
    </div><!-- hidden-xs -->

    <div class="visible-xs">
        <section id="clientes">
            <div class="container">
                <h5>CLIENTES</h5>
                <div class="col-xs-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="col-xs-6 active"><a href="#business-1" aria-controls="business-1" role="tab" data-toggle="tab">Business Partners</a></li>
                        <li role="presentation" class="col-xs-6" ><a href="#integradores-1" aria-controls="integradores-1" role="tab" data-toggle="tab">Integradores</a></li>
                        <li role="presentation" class="col-xs-6"><a href="#fabricantes-1" aria-controls="fabricantes-1" role="tab" data-toggle="tab">Fabricantes</a></li>
                        <li role="presentation" class="col-xs-6" ><a href="#outros-1" aria-controls="outros" role="tab" data-toggle="tab">Outros</a></li>
                    </ul>
                </div><!-- xs-12 -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="business-1">
                        <div class="owl-carousel">
                            <div class="item img">
                                <img class="img-responsive" src="{{ asset('/images/business/business-1.png')}}">
                                <img class="img-responsive" src="{{ asset('/images/business/business-2.png')}}">
                                <img class="img-responsive" src="{{ asset('/images/business/business-3.png')}}">
                                <img class="img-responsive" src="{{ asset('/images/business/business-4.png')}}">
                            </div><!-- item -->
                            <div class="item img">
                                <img class="img-responsive" src="{{ asset('/images/business/business-5.png')}}">
                                <img class="img-responsive" src="{{ asset('/images/business/business-6.jpeg')}}">
                            </div><!-- item -->
                        </div><!-- owl-carousel -->
                    </div><!-- tab-pane -->
                    <div role="tabpanel" class="tab-pane fade" id="integradores-1">
                        <div class="owl-carousel">
                            <div class="item img">
                                <img class="img-responsive" src="{{ asset('/images/integradores/integradores-1.png')}}" >
                                <img class="img-responsive" src="{{ asset('/images/integradores/integradores-2.png')}}" >
                                <img class="img-responsive" src="{{ asset('/images/integradores/integradores-3.png')}}" >
                                <img class="img-responsive" src="{{ asset('/images/integradores/integradores-4.png')}}" >
                            </div><!-- item -->
                            <div class="item img">
                                <img class="img-responsive" src="{{ asset('/images/integradores/integradores-5.png')}}" >
                                <img class="img-responsive" src="{{ asset('/images/integradores/integradores-6.png')}}" >
                            </div><!-- item -->
                        </div><!-- owl-carousel -->
                    </div><!-- tab-pane -->
                    <div role="tabpanel" class="tab-pane fade" id="fabricantes-1">
                       <div class="owl-carousel">
                            <div class="item img">
                                <img class="img-responsive" src="{{ asset('/images/fabricantes/fabricantes-1.png')}}" >
                                <img class="img-responsive" src="{{ asset('/images/fabricantes/fabricantes-2.png')}}">
                                <img class="img-responsive" src="{{ asset('/images/fabricantes/fabricantes-3.png')}}">
                                <img class="img-responsive" src="{{ asset('/images/fabricantes/fabricantes-4.png')}}">
                            </div><!-- item -->
                            <div class="item img">
                                <img class="img-responsive" src="{{ asset('/images/fabricantes/fabricantes-5.png')}}">
                                <img class="img-responsive" src="{{ asset('/images/fabricantes/fabricantes-6.jpeg')}}">
                            </div><!-- item -->
                        </div><!-- owl-carousel -->
                    </div><!-- tab-pane -->
                    <div role="tabpanel" class="tab-pane " id="outros-1">
                        <div class="owl-carousel">
                            <div class="item img">
                                <img class="img-responsive" src="{{ asset('/images/outros/outros-1.jpeg')}}">
                                <img class="img-responsive" src="{{ asset('/images/outros/outros-2.jpeg')}}">
                                <img class="img-responsive" src="{{ asset('/images/outros/outros-3.png')}}">
                                <img class="img-responsive" src="{{ asset('/images/outros/outros-4.png')}}">
                            </div><!-- item -->
                            <div class="item img">
                                <img class="img-responsive" src="{{ asset('/images/outros/outros-5.png')}}">
                                <img class="img-responsive" src="{{ asset('/images/outros/outros-6.jpeg')}}">
                                <img class="img-responsive" src="{{ asset('/images/outros/outros-7.png')}}">
                                <img class="img-responsive" src="{{ asset('/images/outros/outros-8.png')}}">
                            </div><!-- item -->
                        </div><!-- owl-carousel -->
                    </div><!-- tab-pane -->
                </div><!-- tab-content -->
            </div><!-- container -->
        </section><!-- #clientes -->
    </div><!-- visible-xs -->

    <section id="portfolio">
        <div class="container">
            <div class="row">
                <h5>PORTFOLIO</h5>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="img-avaia">
                       <img class="img-avaia-mobile" src="{{ asset('/images/avaya-grande.png')}}">
                    </div><!-- img-avaia -->
                </div><!-- md-3 -->
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="item">
                        <h6>PABX</h6>
                        <p>Avaya Communication Manager, System Manager & Session Manager, Session Border Controller e IPO</p>
                        <h6>Administração</h6>
                        <p>Avaya Control Manager</p>
                        <h6>Conferencia / Video<br> Conferencia</h6>
                        <p>Avaya Aura Conference, Meeting Exchange e Scopia</p>
                    </div><!-- item -->
                </div><!-- md-3 -->
                <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="item">
                        <h6>Correio de Voz</h6>
                        <p>Avaya Aura Messaging, Modular Messaing, Intuity Audix e Voice Mail Pro</p>
                        <h6>Sistema de Multimedia</h6>
                        <p>Avaya Aura Contact Center, Avaya Contact Center Select e Elite Multi Channel</p>
                        <h6>URA</h6>
                        <p>Avaya Experience Portal / Voice Portal</p>
                    </div><!-- item -->
                </div><!-- md-3 -->
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="item">
                        <h6>CTI</h6>
                        <p>Avaya Enablement Services</p>
                        <h6>Sistema de Relatórios</h6>
                        <p>Call Management System e Customer Call Reporter</p>
                        <h6>Gravação</h6>
                        <p>Workforce Optimization</p>
                    </div><!-- item -->
                </div><!-- md-3 -->
            </div><!-- row -->
            <div class="borda"></div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="img-micro">
                                <img src="{{ asset('/images/Microsoft-logo.png')}}">
                            </div><!-- img-micro -->
                        </div><!-- md-4 -->
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="conteudo">
                                <div class="centralizar">
                                    <h6>Sistema Operacional</h6>
                                    <p>Windows 2008 / 2012</p>
                                    <span>Instalação de DNS, HTTP Server e DHCP</span>
                                </div><!-- centralizar -->
                            </div><!-- conteudo -->
                        </div><!-- md-8 -->
                    </div><!-- row -->
                </div><!-- md-6 -->
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="item-img">
                          <img src="{{ asset('/images/vmware.png')}}">
                        </div><!-- item-img -->
                    </div><!-- md-6 -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="texto item-img">
                            <h6>Virtualização</h6>
                            <span>ESXi + VMHA</span>
                        </div><!-- texto -->
                    </div><!-- md-6 -->
                </div><!-- md-6 -->
            </div><!-- row -->
        </div><!-- container-->
    </section><!-- #portfolio -->

    <section id="contato">
        <div class="container">
           <div class="row">
                <h5>CONTATO</h5>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="conteudo">
                        <h6>Horário de atendimento</h6>
                        <p>Seg a Sex - 09:00 - 18:00 </p>
                        <p>Sáb e Dom - Não atendemos</p>
                    </div><!-- conteudo -->
                    <div class="fone">
                        <img src="{{ asset('/images/Relogio.png') }}">
                    </div><!-- fone -->
                </div><!-- md-6 -->
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="conteudo">
                        <h6>Telefone e E-mail</h6>
                        <p>Telefone - +55 11 3042-2365</p>
                        <p>email@email.com.br</p>
                    </div><!-- conteudo -->
                    <div class="fone">
                        <img src="{{ asset('/images/fone.png')}}">
                    </div><!-- fone -->
                </div><!-- md-6 -->
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                    <form method="post" action="/enviar_dados_contato">
                    @csrf
                        <div class="item" id="item-1">
                            <div class="borda-externa">
                            <button type="button" class="btn btn-default pull-right btn-proximo-direita" id="btn-avancar-1"><span class="glyphicon glyphicon-chevron-right seta"></span></button>
                                <div id="inserir" class="centralizar">
                                    <div class="borda-interna">
                                        <form class="form-horizontal">

                                            <h6> Vamos Conversar</h6>
                                            <p>Primeiro, diga-nos sobre o que gostaria de falar:</p>

                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="tema" id="optionsRadios1" value="Quero saber mais sobre o suporte" checked>
                                                    Quero saber mais sobre o suporte
                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="tema" id="optionsRadios1" value="Tenho uma sugestão a fazer" checked>
                                                    Tenho uma sugestão a fazer
                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="tema" id="optionsRadios1" value="Quero fazer uma reclamação" checked>
                                                    Quero fazer uma reclamação
                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="tema" id="optionsRadios1" value="Quero fazer uma reclamação" checked>
                                                    Quero fazer uma reclamação
                                                </label>
                                            </div>
                                    </div>

                                </div>
                                <span> 1/4</span>
                                <div class="borda-rolagem"></div>
                            </div>
                        </div><!-- item -->

                        <div class="item" id="item-2">
                            <div class="borda-externa">
                                <button type="button" class="btn btn-default pull-right btn-proximo-direita" id="btn-avancar-2"><span class="glyphicon glyphicon-chevron-right seta"></span></button>
                                <button type="button" class="btn btn-default btn-proximo-esquerda" id="btn-voltar-2"><span class="glyphicon glyphicon-chevron-left seta"></span></button>
                                <div class="centralizar">

                                    <h6> Vamos Conversar</h6>
                                    <p class="paragrafo2">Ok! Como você se chama?</p>
                                    <input  class="input" type="text"  id="nome" placeholder="Digite seu nome" name="name">

                                </div>
                                <span> 2/4</span>
                                <div class="borda-rolagem2"></div>
                            </div>
                        </div>

                        <div class="item" id="item-3">
                            <div class="borda-externa">
                                <button type="button" class="btn btn-default pull-right btn-proximo-direita" id="btn-avancar-3"><span class="glyphicon glyphicon-chevron-right seta"></span></button>
                                <button type="button" class="btn btn-default btn-proximo-esquerda" id="btn-voltar-3"><span class="glyphicon glyphicon-chevron-left seta"></span></button>
                                <div class="centralizar">
                                    <h6> Vamos Conversar</h6>
                                    <p>Muito prazer! Diga-nos o que você tem em mente:</p>
                                    <textArea id="mensagem" class="form-control" rows="4" placeholder="Mensagem" name="message"></textArea>

                                </div>
                                <span> 3/4</span>
                                <div class="borda-rolagem3"></div>
                            </div>
                        </div>

                        <div class="item" id="item-4">
                            <div class="borda-externa">
                                <button type="button" class="btn btn-default btn-proximo-esquerda" id="btn-voltar-4"><span class="glyphicon glyphicon-chevron-left seta"></span></button>
                                <div class="centralizar">
                                    <h6> Vamos Conversar</h6>
                                    <p>Como podemos entrar em contato com você?</p>
                                    <input type="email"  id="email" class="input-slyde4" placeholder="email@email.com.br" name="email">
                                    <input type="tel"  id="email" class="input2-slyde4" placeholder="(00)0000-0000" name="phone">

                                        <div><button class="btn btn-default btn" type="submit">ENVIAR</button></div>
                                    </form>
                                </div>
                                <span> 4/4</span>
                                <div class="borda-rolagem4"></div>
                            </div>
                        </div>
                    </form>
                </div><!-- md-6 -->
            </div><!-- row -->
        </div><!-- container -->
    </section><!-- #contato -->

@include('footer')